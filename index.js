"use strict";

const tabsTitle = document.querySelectorAll(".tab-title");
const tabsContent = document.querySelectorAll(".tab-content");
console.log(tabsContent);

tabsTitle.forEach((title) => {
  title.addEventListener("click", (event) => {
    const tab = event.target.dataset.tab;

    tabsContent.forEach((content) => {
      if (content.dataset.tab === tab) {
        content.classList.add("active");
      } else {
        content.classList.remove("active");
      }
    });

    tabsTitle.forEach((title) => {
      if (title.dataset.tab === tab) {
        title.classList.add("active");
      } else {
        title.classList.remove("active");
      }
    });
  });
});
